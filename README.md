# OpenML dataset: emotions

https://www.openml.org/d/41545

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multi-label dataset. Audio dataset (emotions) consists of 593
  musical files with 6 clustered emotional labels and 72 predictors. Each song
  can be labeled with one or more of the labels {amazed-surprised,
  happy-pleased, relaxing-calm, quiet-still, sad-lonely, angry-aggressive}.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41545) of an [OpenML dataset](https://www.openml.org/d/41545). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41545/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41545/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41545/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

